---
title: "About me"
date: "2021-05-10"
author: "Fabien Schlegel"
path: "/about"
---

Hi, my name is Fabien. I'm a french web developper and I work with Javascript, Typescript and Python.

First of all, thank you for reading my blog. I create it to share my knowledges with everyone.

If you like my content, you can share it or follow me on [Twitter](https://twitter.com/fabienschlegel).
